'use strict';

var http = require('http'),
	carSchema  = require('../models/car')

function saveAcm(req, res){

let idInicio = ''


if (req.query.idMxp) {
	idInicio= req.query.idMxp
}else{
	res.json({'status' :  404})
}

let fecha = new Date()
let  postData = {}

postData['published_sites.acm.id']  = ''
postData['published_sites.acm.url'] = ''
postData['published_sites.acm.date_delete'] = fecha
postData['published_sites.acm.status'] = 'closed'
postData['published_sites.acm.action'] = 'nothing'
postData['published_sites.acm.comment'] = 'El anuncio ha sido cerrado exitosamente'

console.log('Eliminando este idmxp', req.query.idMxp)
  let idcar            = req.query.idMxp
  let siteId           = 'acm'
  let published_sites  = `published_sites.${siteId}`

if (req.query.status == 'deleted') {

	carSchema.findOneAndUpdate({_id: parseInt(idInicio)}, {$set: postData},function(err, doc){

				    if(err){
				        console.log(doc);
				        res.json({'status' : 404})
				    }else{
					console.log(doc)
				    	res.json({'status' : 200 })
				    }

				});


}else{

  carSchema.findOne( {_id : idcar }, published_sites , function(err,car){
        if (err) {
          res.json({'status' : 404})
        }else{
          let indice = `historical.${siteId}`

          let historical = {}


          historical[indice] = {
            url             : car.published_sites[siteId].url,
            id              : car.published_sites[siteId].id,
            dateCreate      : car.published_sites[siteId].date_create,
            dateExpiration  : car.published_sites[siteId].date_expiration == null ? '' : car.published_sites[siteId].date_expiration ,
            visits          : car.published_sites[siteId].visits,
            leads           : car.published_sites[siteId].leads,
            calls           : car.published_sites[siteId].calls,
            levelAdd        : car.published_sites[siteId].level_add,
            dateUpdate      : car.published_sites[siteId].date_update,
          }
          
           carSchema.update({_id: idcar}, { '$push' :historical} ,  function (err, doc) 
          {
            if (err) 
            {
              res.json({'status' : 404})
            }
            else
            {
              carSchema.findOneAndUpdate({_id: parseInt(idInicio)}, {$set: postData},function(err, doc){

				    if(err){
				        console.log(doc);
				        res.json({'status' : 404})
				    }else{
					console.log(doc)
				    	res.json({'status' : 200 })
				    }

				});
            }
          });


         

        }
  });


}
}




module.exports.saveAcm = saveAcm



