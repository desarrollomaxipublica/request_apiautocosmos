'use strict';

var database = require('../database'),
mongoose = require('mongoose'),
Schema = mongoose.Schema;

var carSchema = new Schema({
    _id 	: {type : Number},
    status :  {type:String, required: false},
    dealer : {
          dealer_ID: { type: String, requiered : false }
    },
    published_sites: {

      mlm : {
            id        : {type : String, required : false},
            url       : {type : String, required : false},
            status    : {type : String, required : false},
            action    : {type : String, required : false},
            visits    : {type : String, required : false},
            leads     : {type : String, required : false},
            calls     : {type : String, required : false},
            comment   : {type : String, required : false},
            level_add : {type : String, required : false},
            date_create : {type : Date, required : false},
            date_update : {type : Date, required : false},
            date_expiration : {type : Date, required : false, default: ''},
            date_delete : {type : String, required : false},
            try       : {type : Number, required : false}

        },
      smm : {
            id        : {type : String, required : false},
            url       : {type : String, required : false},
            status    : {type : String, required : false},
            action    : {type : String, required : false},
            visits    : {type : String, required : false},
            leads     : {type : String, required : false},
            calls     : {type : String, required : false},
            comment   : {type : String, required : false},
            level_add : {type : String, required : false},
            date_create : {type : Date, required : false},
            date_update : {type : Date, required : false},
            date_expiration : {type : Date, required : false},
            date_delete : {type : String, required : false,  default: ''},
            try       : {type : Number, required : false},
            code      : {type : String, required : false}

          },
          vam : {
                id        : {type : String, required : false},
                url       : {type : String, required : false},
                status    : {type : String, required : false},
                action    : {type : String, required : false},
                visits    : {type : String, required : false},
                leads     : {type : String, required : false},
                calls     : {type : String, required : false},
                comment   : {type : String, required : false},
                level_add : {type : String, required : false},
                date_create : {type : Date, required : false},
                date_update : {type : Date, required : false},
                date_expiration : {type : Date, required : false},
                date_delete : {type : String, required : false},
                try       : {type : Number, required : false}

            },

            dmm : {
                  id        : {type : String, required : false},
                  url       : {type : String, required : false},
                  status    : {type : String, required : false},
                  action    : {type : String, required : false},
                  visits    : {type : String, required : false},
                  leads     : {type : String, required : false},
                  calls     : {type : String, required : false},
                  comment   : {type : String, required : false},
                  level_add : {type : String, required : false},
                  date_create : {type : Date, required : false},
                  date_update : {type : Date, required : false},
                  date_expiration : {type : Date, required : false},
                  date_delete : {type : String, required : false},
                  try       : {type : Number, required : false}

              },
              acm : {
                    id        : {type : String, required : false},
                    url       : {type : String, required : false},
                    status    : {type : String, required : false},
                    action    : {type : String, required : false},
                    visits    : {type : String, required : false},
                    leads     : {type : String, required : false},
                    calls     : {type : String, required : false},
                    comment   : {type : String, required : false},
                    level_add : {type : String, required : false},
                    date_create : {type : Date, required : false},
                    date_update : {type : Date, required : false},
                    date_expiration : {type : Date, required : false},
                    date_delete : {type : String, required : false},
                    try       : {type : Number, required : false}

                },
                aam : {
                      id        : {type : String, required : false},
                      url       : {type : String, required : false},
                      status    : {type : String, required : false},
                      action    : {type : String, required : false},
                      visits    : {type : String, required : false},
                      leads     : {type : String, required : false},
                      calls     : {type : String, required : false},
                      comment   : {type : String, required : false},
                      level_add : {type : String, required : false},
                      date_create : {type : Date, required : false},
                      date_update : {type : Date, required : false},
                      date_expiration : {type : Date, required : false},
                      date_delete : {type : String, required : false},
                      try       : {type : Number, required : false}

                  },

                  mys : {
                        id        : {type : String, required : false},
                        url       : {type : String, required : false},
                        status    : {type : String, required : false},
                        action    : {type : String, required : false},
                        visits    : {type : String, required : false},
                        leads     : {type : String, required : false},
                        calls     : {type : String, required : false},
                        comment   : {type : String, required : false},
                        level_add : {type : String, required : false},
                        date_create : {type : Date, required : false},
                        date_update : {type : Date, required : false},
                        date_expiration : {type : Date, required : false},
                        date_delete : {type : String, required : false},
                        try       : {type : Number, required : false}

                    },

                    sam : {
                          id        : {type : String, required : false},
                          url       : {type : String, required : false},
                          status    : {type : String, required : false},
                          action    : {type : String, required : false},
                          visits    : {type : String, required : false},
                          leads     : {type : String, required : false},
                          calls     : {type : String, required : false},
                          comment   : {type : String, required : false},
                          level_add : {type : String, required : false},
                          date_create : {type : Date, required : false},
                          date_update : {type : Date, required : false},
                          date_expiration : {type : Date, required : false},
                          date_delete : {type : String, required : false},
                          try       : {type : Number, required : false}

                      }

    },

   historical: {
    smm: [
      {
        _id             : false,
        url             : {type : String, required : true},
        id              : {type : String, index: { unique: true }, required : true},
        dateCreate      : {type : Date, required : false},
        dateExpiration  : {type : Date, required : false},
        visits          : {type : String, required : false},
        leads           : {type : String, required : false},
        calls           : {type : String, required : false},
        levelAdd        : {type : String, required : false},
        dateUpdate      : {type : String, required : false},
        dateHistorical  : {type : Date, required:true, default :  Date.now}
      }

  ],
    mlm: [
      {
        _id             : false,
        url             : {type : String, required : true},
        id              : {type : String, index: { unique: true }, required : true},
        dateCreate      : {type : Date, required : false},
        dateExpiration  : {type : Date, required : false},
        visits          : {type : String, required : false},
        leads           : {type : String, required : false},
        calls           : {type : String, required : false},
        levelAdd        : {type : String, required : false},
        dateUpdate      : {type : String, required : false},
        dateHistorical  : {type : Date, required:true, default :  Date.now}
      }

  ],
  acm: [
      {
        _id             : false,
        url             : {type : String, required : true},
        id              : {type : String, index: { unique: true }, required : true},
        dateCreate      : {type : Date, required : false},
        dateExpiration  : {type : Date, required : false},
        visits          : {type : String, required : false},
        leads           : {type : String, required : false},
        calls           : {type : String, required : false},
        levelAdd        : {type : String, required : false},
        dateUpdate      : {type : String, required : false},
        dateHistorical  : {type : Date, required:true, default :  Date.now}
      }

  ],
  dmm: [
      {
        _id             : false,
        url             : {type : String, required : true},
        id              : {type : String, index: { unique: true }, required : true},
        dateCreate      : {type : Date, required : false},
        dateExpiration  : {type : Date, required : false},
        visits          : {type : String, required : false},
        leads           : {type : String, required : false},
        calls           : {type : String, required : false},
        levelAdd        : {type : String, required : false},
        dateUpdate      : {type : String, required : false},
        dateHistorical  : {type : Date, required:true, default :  Date.now}
      }

  ],
  sam: [
      {
        _id             : false,
        url             : {type : String, required : true},
        id              : {type : String, index: { unique: true }, required : true},
        dateCreate      : {type : Date, required : false},
        dateExpiration  : {type : Date, required : false},
        visits          : {type : String, required : false},
        leads           : {type : String, required : false},
        calls           : {type : String, required : false},
        levelAdd        : {type : String, required : false},
        dateUpdate      : {type : String, required : false},
        dateHistorical  : {type : Date, required:true, default :  Date.now}
      }

  ],
  aam: [
      {
        _id             : false,
        url             : {type : String, required : true},
        id              : {type : String, index: { unique: true }, required : true},
        dateCreate      : {type : Date, required : false},
        dateExpiration  : {type : Date, required : false},
        visits          : {type : String, required : false},
        leads           : {type : String, required : false},
        calls           : {type : String, required : false},
        levelAdd        : {type : String, required : false},
        dateUpdate      : {type : String, required : false},
        dateHistorical  : {type : Date, required:true, default :  Date.now}
      }

  ]


}

});

var car = mongoose.model('car', carSchema,'car');
module.exports = car;
